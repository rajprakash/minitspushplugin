package com.minits.push;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Person;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.service.notification.StatusBarNotification;
import android.support.v4.app.NotificationCompat;

import com.onesignal.OSNotificationDisplayedResult;
import com.onesignal.OSNotificationPayload;
import com.onesignal.NotificationExtenderService;
import com.onesignal.OSNotificationReceivedResult;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.List;

import android.support.v4.app.NotificationCompatSideChannelService;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

public class MinitsNotificationExtenderService extends NotificationExtenderService {

    @Override
    protected boolean onNotificationProcessing(OSNotificationReceivedResult receivedResult) {

        JSONObject additionalData = receivedResult.payload.additionalData;
        if (!additionalData.has("groupid")) {
            return false;
        }

        OverrideSettings overrideSettings = new OverrideSettings();
        Context context = getApplicationContext();
        overrideSettings.extender = new NotificationCompat.Extender() {
            @Override
            public NotificationCompat.Builder extend(NotificationCompat.Builder builder) {
                String groupId = "";
                String groupName = "";
                String name = "";
                String userId = "";
                String body = receivedResult.payload.body;
                int msgLength = 0;
                try {
                    groupName = additionalData.getString("group_name");
                    name = additionalData.getString("name");
                    groupId = additionalData.getString("groupid");
                    userId = additionalData.getString("userId");
                } catch (JSONException e) {
                    Log.e("MYAPP", "unexpected JSON exception", e);
                    // Do something to recover ... or kill the app.
                }
                if (Build.VERSION.SDK_INT >= 24) {
                    NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                    StatusBarNotification sn[] = nm.getActiveNotifications();
                    for (StatusBarNotification ele : sn) {
                        if (ele.getPackageName().toString().equals(context.getPackageName().toString())) {
                            String groupKey = ele.getGroupKey();
                            List<String> arr = Arrays.asList(groupKey.split("[|]"));
                            String previousKey = arr.get(arr.size() - 1);
                            String currentKey = "g:" + groupId;
                            if (previousKey.equals(currentKey)) {
                                Notification previousNotification = ele.getNotification();
                                NotificationCompat.MessagingStyle activeStyle = NotificationCompat.MessagingStyle.extractMessagingStyleFromNotification(previousNotification);
                                Notification.Builder recoveredBuilder = Notification.Builder.recoverBuilder(context, previousNotification);
                                NotificationCompat.MessagingStyle messagingStyleStyle = new NotificationCompat.MessagingStyle("Me");
                                messagingStyleStyle.setConversationTitle(groupName);
                                List<NotificationCompat.MessagingStyle.Message> messages = activeStyle.getMessages();
                                msgLength = messages.size();
                                for (NotificationCompat.MessagingStyle.Message msg : messages) {
                                    messagingStyleStyle.addMessage(msg.getText(), msg.getTimestamp(), msg.getSender());
                                }
                                messagingStyleStyle.addMessage(body, System.currentTimeMillis(), name);
                                builder.setContentTitle(name)
                                        .setContentText(body)
                                        .setAutoCancel(true)
                                        .setNumber(msgLength)
                                        .setColor(0xff11acfa)
                                        .setGroupSummary(false)
                                        .setShowWhen(true)
                                        .setStyle(messagingStyleStyle)
                                        .setGroup(groupId);
                                return builder;
                            }
                        }

                    }
                }


                NotificationCompat.MessagingStyle messagingStyleStyle = new NotificationCompat.MessagingStyle("Me");
                messagingStyleStyle.setConversationTitle(groupName)
                        .addMessage(body, System.currentTimeMillis(), name);
                builder.setContentTitle(name)
                        .setContentText(body)
                        .setAutoCancel(true)
                        .setNumber(msgLength)
                        .setColor(0xff11acfa)
                        .setGroupSummary(false)
                        .setShowWhen(true)
                        .setStyle(messagingStyleStyle)
                        .setGroup(groupId);
                return builder;
            }
        };

        OSNotificationDisplayedResult displayedResult = displayNotification(overrideSettings);
        Log.d("OneSignalExample", "Notification displayed with id: " + displayedResult.androidNotificationId);
//        displayedResult.notify();
        // Return true to stop the notification from displaying.
        return true;
    }

}